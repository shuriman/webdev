function loadScript(url, callback) {
   
    const element = document.createElement("script");
    element.type = "text/javascript";
    element.src = url;
    element.onload = callback;

    document.body.appendChild(element);
    
}

export function loadScriptEx(a, callback) {

    if (typeof a === 'function') {
        a();
    }
    else{
        if(!Array.isArray(a)){
            loadScript(a, callback)
        }else{
            a.forEach((url, callback) => {
                loadScript(url, callback)
            });
        }
    }
}