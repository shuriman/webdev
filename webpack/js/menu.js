import {loadScriptEx} from './load.js';
import {printError, printResult} from './printResult.js';
import { _Howl as Howl } from "./howler.js";

let defaultMode = 0;

const showMain = () =>{
    switch(defaultMode){
        case 0:{
            showDateCalc();
            break;
        }
        case 1:{
            showTimer();
            break;
        }
        default:{
            showDateCalc();
            break;
        }
    }
}

const showTimer = () =>{
    let mainDiv = document.getElementById("main");  
    mainDiv.innerHTML = `<div id="timer">
            <h3>Таймер</h3>
            <label>
                <strong>Дата:</strong>
                <input id="timerDate" type="time">
            </label>
            <button id="startbtn">Start</button>
            <button id="stopbtn">Stop</button>
            <p id="__result"></p>
        </div>
    `;

    let nIntervId;
    let nTimer = 0;

    const showTime = () => {
        const time = nTimer++;

        const stopTimeElement = document.getElementById("timerDate");
        if(stopTimeElement != null){
            const stopTime  = new Date(new Date().toDateString() + ' ' + stopTimeElement.value);
            if(stopTime.getTime() < new Date().getTime())
                stopTimer();
            console.log(stopTime);
        }

        printResult(time);

    }

    const startTimer = () => {
        if (!nIntervId) {
            nIntervId = setInterval(showTime, 1000);
        }
    }

    const stopTimer = () => {
        clearInterval(nIntervId);

        var sound = new Howl({
            src: ['sound.mp3']
        });
        sound.play();

        nIntervId = null; 
    }

    const handlerStart = (event) => {
        event.target.disabled = true;
        startTimer();
        stopbtn.disabled = false;
    }

    const handlerStop = (event) =>{
        event.target.disabled = true;
        stopTimer();
        startbtn.disabled = false;
    }

    const startbtn = document.getElementById("startbtn");
    startbtn.addEventListener("click", handlerStart);

    const stopbtn = document.getElementById("stopbtn");
    stopbtn.addEventListener("click", handlerStop);
    //loadScriptEx("src/timer.js", () => {console.log('loaded!')});
}

const showDateCalc = () =>{
    let mainDiv = document.getElementById("main");
   
    mainDiv.innerHTML = `<form id="datecalc">
        <h3>Калькулятор дат</h3>
        <label>
            <strong>Первая дата:</strong>
            <input type="date" name="firstDate" />
        </label>
        <label>
            <strong>Вторая дата:</strong>
            <input type="date" name="secondDate" />
        </label>
        <button type="submit">Расчитать промежуток</button>
        <p id="__result"></p>
    </form>`;

    const dateCalcForm = document.getElementById("datecalc");

    const handleCalcDates = (event) => {

        event.preventDefault();

        let { firstDate, secondDate } = event.target.elements;
        firstDate = firstDate.value, secondDate = secondDate.value;

        if (firstDate && secondDate) printResult(`${firstDate}, ${secondDate}`);
        else printError("Для расчета промежутка необходимо заполнить оба поля");
    }

    dateCalcForm.addEventListener("submit", handleCalcDates);

    //loadScriptEx("js/main.js", () => {console.log('loaded!')});

}

const handleTimerClick = () => {
    showTimer();
}

const handleCalcClick = () => {
    showDateCalc();
}

const timerbtn = document.getElementById("timerbtn");
timerbtn.addEventListener("click", handleTimerClick);

const calcbtn = document.getElementById("calcbtn");
calcbtn.addEventListener("click", handleCalcClick);

showMain();