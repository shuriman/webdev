import {printResult} from './printResult.js';
import { _Howl as Howl } from "./howler.js";

let nIntervId;
let nTimer = 0;

const showTime = () => {
    const time = nTimer++;

    const stopTimeElement = document.getElementById("timerDate");
    if(stopTimeElement != null){
        const stopTime  = new Date(new Date().toDateString() + ' ' + stopTimeElement.value);
        if(stopTime.getTime() < new Date().getTime())
            stopTimer();
        console.log(stopTime);
    }

    printResult(time);

}

const startTimer = () => {
    if (!nIntervId) {
        nIntervId = setInterval(showTime, 1000);
    }
}

const stopTimer = () => {
    clearInterval(nIntervId);

    var sound = new Howl({
        src: ['sound.mp3']
    });
    sound.play();

    nIntervId = null; 
}

const handlerStart = (event) => {
    event.target.disabled = true;
    startTimer();
    stopbtn.disabled = false;
}

const handlerStop = (event) =>{
    event.target.disabled = true;
    stopTimer();
    startbtn.disabled = false;
}

const startbtn = document.getElementById("startbtn");
startbtn.addEventListener("click", handlerStart);

const stopbtn = document.getElementById("stopbtn");
stopbtn.addEventListener("click", handlerStop);