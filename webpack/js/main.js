import {printError, printResult} from './printResult.js';

const dateCalcForm = document.getElementById("datecalc");

const handleCalcDates = (event) => {

    event.preventDefault();

    let { firstDate, secondDate } = event.target.elements;
    firstDate = firstDate.value, secondDate = secondDate.value;

    if (firstDate && secondDate) printResult(`${firstDate}, ${secondDate}`);
    else printError("Для расчета промежутка необходимо заполнить оба поля");
}

dateCalcForm.addEventListener("submit", handleCalcDates);